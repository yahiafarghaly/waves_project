% In this script, we calculate both incident & scattered fields @ Cylinder's
% surface to confirm the boundary conditions are met.

clc,clear,close all

%% Initialize Parameters
freq = 3*10^9;
lamda = (3*10^8)/freq; % in meter
beta = 2*pi/lamda;
uo = 1.256637061*10^-6;

num_sum = 100;
range = -num_sum:num_sum;

% Calculate Coefficients
radius = 1;
C_n =  -1.*besselj(range, beta*radius) ./ besselh(range, 2, beta*radius);

%% Confirm that @ roi = radius, E_incident + E_scattered = 0
roi = 1;
num_points = 300;   % number of points to calculate the field
phi = linspace(0, 2*pi, num_points);

for i = 1 : length(phi)
    E_incident(i) = calculate_E_incident(beta, roi, phi(i), range);
    E_scattered(i) = calculate_E_scattered(radius,beta,roi, phi(i),range);
end

difference = abs(sum(E_incident+E_scattered))