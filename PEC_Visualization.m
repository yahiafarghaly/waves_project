% In this script, we calculate both magnetic & electric fields around the
% cylinder & visualize the magnitudes of the fields.

clc,clear,close all

%% Initialize Parameters
freq = 3*10^9;
lamda = (3*10^8)/freq; % in meter
beta = 2*pi/lamda;
uo = 1.256637061*10^-6;

num_sum = 200;
range = -num_sum:num_sum;

%% Calculate Coefficients
radius = 1;
for n = range
    C_n(n+abs(min(range))+1) = -1* besselj(n, beta*radius) / besselh(n, 2, beta*radius);
end


%% Calculate Electric & Magnetic Fields around the cylinder
resolution = 30;
r = linspace(radius, 10*radius, resolution);
phi = linspace(0, 2*pi, resolution);
[R PHI] = meshgrid(r,phi);

const_H_phi = beta/(1j*2*pi*freq*uo);
const_H_roi = -1/(1j*2*pi*freq*uo); 

for i = 1:resolution
   for j = 1:resolution
       E_incident(i, j) = calculate_E_incident(beta, R(i, j), PHI(i, j), range);
       E_scattered(i, j) = calculate_E_scattered( C_n, beta, R(i, j), PHI(i, j),range);
       H_phi(i, j) = calculate_H_phi( C_n, beta, R(i, j), PHI(i, j),range, const_H_phi);
       H_roi(i, j) = calculate_H_roi( C_n, beta, R(i, j), PHI(i, j),range, const_H_roi);
   end
end

% Visualize the Electric & Magnetic Fields
figure;
subplot(2,2,1)
surf(R.*cos(PHI), R.*sin(PHI), abs(E_incident));
title('E_{incident}')
subplot(2,2,2)
surf(R.*cos(PHI), R.*sin(PHI), abs(E_scattered));
title('E_{scattered}')
subplot(2,2,3)
surf(R.*cos(PHI), R.*sin(PHI), abs(H_phi));
title('H_{phi}')
subplot(2,2,4)
surf(R.*cos(PHI), R.*sin(PHI), abs(H_roi));
title('H_{roi}')

