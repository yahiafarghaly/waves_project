function [ H_phi ] = calculate_H_phi( C_n, beta,roi,phi,range, const)
dbesselj_coefficients = dbesselj(range, beta*roi); %bessel
dbesselh_coefficients = dbesselh(range, 2, beta*roi); %hankel
H_phi = const * sum( (1j).^-range .* (dbesselj_coefficients+ C_n.*dbesselh_coefficients) .*exp(1j*phi(1).*range));
end

