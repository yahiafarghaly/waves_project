function [ E_incident ] = calculate_E_incident( beta,roi,phi,range)
bessel_coefficients = besselj(range, beta*roi);
E_incident = sum((1j).^-range .*bessel_coefficients.*exp(1j*phi.*range));
end

