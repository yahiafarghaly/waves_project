function [ E_scattered ] = calculate_E_scattered( C_n, beta,roi,phi,range)
hankel_coefficients = besselh(range, 2, beta*roi); % k = 2
E_scattered = sum((1j).^-range .* C_n.*hankel_coefficients.*exp(1j*phi.*range));
end