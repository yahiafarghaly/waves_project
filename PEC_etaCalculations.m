% In this script, we are calculating the ratio between the magnetic 
% & electric fields, we are expecting a result of ~ 120*pi = 376.730, 
% increasing the summation terms should get us closer to that. Note that at  
% high number of summations, the bessel function starts producing NaNs.

clc,clear,close all

%% Initialize Parameters
freq = 3*10^9;
lamda = (3*10^8)/freq; % in meter
beta = 2*pi/lamda;
uo = 1.256637061*10^-6;
const_H_phi = beta/(1j*2*pi*freq*uo);   % used in H_phi function
const_H_roi = -1/(1j*2*pi*freq*uo);     % used in H_roi function

%% Eta Calculations

num_sum = 100;
range = -num_sum:num_sum;

% Calculate Coefficients
radius = 1;
for n = range
    C_n(n+abs(min(range))+1) = -1* besselj(n, beta*radius) / besselh(n, 2, beta*radius);
end

resolution = 30;
phi_range = linspace(0, 2*pi, resolution);
roi_range = linspace(radius, 100*radius, resolution);
for j = 1:resolution
    phi = phi_range(j);
    for i = 1:resolution
        roi = roi_range(i);
        E_incident(j,i) = calculate_E_incident(beta, roi, phi, range);
        E_scattered(j,i) = calculate_E_scattered( C_n, beta, roi, phi,range);
        H_phi(j,i) = calculate_H_phi( C_n, beta, roi, phi,range, const_H_phi);
        H_roi(j,i) = calculate_H_roi( C_n, beta, roi, phi,range, const_H_roi);
        eta(j,i) = abs(E_incident(j,i)+E_scattered(j,i)) ./ abs(H_phi(j,i));
        % Note: adding +H_roi(j, i) to the summation had little effect on the value if eta;
    end
end

figure
surf(roi_range,rad2deg(phi_range),eta)
xlabel('roi')
ylabel('phi(in degree)')
zlabel('eta')

for j = 1:resolution
    avg_eta(j) = sum(eta(j,:))/resolution;
    %fprintf('Average Value of eta = %f\n',sum(eta(j,:))/length(eta(j,:)));
    %fprintf('diff = %f\n',abs(120*pi - sum(eta(j,:))/length(eta(j,:))));
end

figure
bar(rad2deg(phi_range),avg_eta)
hold on
plot(rad2deg(phi_range), repmat(120*pi,1,resolution),'r');
legend('average eta along roi in a certain phi','120*pi')
ylim([ 0 700])
xlim([0 360])
xlabel('phi')
ylabel('average eta')

% Note: The avg value is close to what we need, we should find an
% explanation for the discontinuities and the large jumps in between