function [ H_phi ] = calculate_H_roi( C_n, beta,roi,phi,range, const)
bessel_coefficients = besselj(range, beta*roi);
hankel_coefficients = besselh(range, 2, beta*roi); % k = 2
H_phi = const/roi * sum( range.*(1j).^(-range+1) .* (bessel_coefficients+ C_n.*hankel_coefficients) .*exp(1j*phi(1).*range));
end

